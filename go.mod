module gitlab.com/crdc/apex/go-apex

require (
	github.com/go-kit/kit v0.8.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/kisielk/gotool v1.0.0 // indirect
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/pebbe/zmq4 v1.0.0
	golang.org/x/net v0.0.0-20190213061140-3a22650c66bd
	golang.org/x/sys v0.0.0-20190318195719-6c81ef8f67ca // indirect
	google.golang.org/genproto v0.0.0-20190307195333-5fe7a883aa19 // indirect
	google.golang.org/grpc v1.19.0
)
