package log

import (
	"errors"
	"testing"

	"github.com/go-kit/kit/log/level"
)

func TestLogger(t *testing.T) {
	logger := GetLogger()

	SetLevel(logger, "debug")
	level.Debug(*logger).Log("msg", "debug test")
	Debug(logger, "msg", "debug test")

	SetLevel(logger, "info")
	level.Info(*logger).Log("event", "info test")
	Info(logger, "event", "info test")

	SetLevel(logger, "warn")
	level.Warn(*logger).Log("msg", "warn test")
	Warn(logger, "msg", "warn test")

	SetLevel(logger, "error")
	level.Error(*logger).Log("err", errors.New("error test"))
	Error(logger, "err", errors.New("error test"))
}
