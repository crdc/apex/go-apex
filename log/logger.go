package log

import (
	"os"
	"sync"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

var instance *log.Logger
var once sync.Once

func GetLogger() *log.Logger {
	once.Do(func() {
		logger := log.NewJSONLogger(log.NewSyncWriter(os.Stdout))
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = level.NewFilter(logger, level.AllowInfo())
		instance = &logger
	})
	return instance
}

func SetLevel(logger *log.Logger, logLevel string) {
	switch logLevel {
	case level.DebugValue().String():
		*logger = log.With(*logger, "caller", log.DefaultCaller)
		*logger = level.NewFilter(*logger, level.AllowAll())
		break
	case level.ErrorValue().String():
		*logger = level.NewFilter(*logger, level.AllowError())
		break
	case level.InfoValue().String():
		*logger = level.NewFilter(*logger, level.AllowInfo())
		break
	case level.WarnValue().String():
		*logger = level.NewFilter(*logger, level.AllowWarn())
		break
	default:
		*logger = level.NewFilter(*logger, level.AllowError())
		break
	}
}

func Debug(logger *log.Logger, keyvals ...interface{}) {
	level.Debug(*logger).Log(keyvals...)
}

func Error(logger *log.Logger, keyvals ...interface{}) {
	level.Error(*logger).Log(keyvals...)
}

func Info(logger *log.Logger, keyvals ...interface{}) {
	level.Info(*logger).Log(keyvals...)
}

func Warn(logger *log.Logger, keyvals ...interface{}) {
	level.Warn(*logger).Log(keyvals...)
}
