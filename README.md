# Apex Go Library

[![Build Status](https://gitlab.com/crdc/apex/go-apex/badges/master/build.svg)](https://gitlab.com/crdc/apex/go-apex/commits/master)
[![Coverage Report](https://gitlab.com/crdc/apex/go-apex/badges/master/coverage.svg)](https://gitlab.com/crdc/apex/go-apex/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/crdc/apex/go-apex)](https://goreportcard.com/report/gitlab.com/crdc/apex/go-apex)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

[WIP] Components shared across various services and tools.

## Reliable Request-Reply API

## Message Types and Serialization

## Service Base

## Utility Functions
