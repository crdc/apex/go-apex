package main

import (
	"fmt"

	"gitlab.com/crdc/apex/go-apex/message"
)

func main() {
	s := message.StatusRequest{}
	bytes, _ := s.ToJSON()
	fmt.Println(string(bytes))
}
