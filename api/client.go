// Majordomo Protocol Client API.
// Implements the MDP/Worker spec at http://rfc.zeromq.org/spec:7.
package api

import (
	"errors"
	"runtime"
	"time"

	"gitlab.com/crdc/apex/go-apex/log"
	//"gitlab.com/crdc/apex/go-apex/util"

	zmq "github.com/pebbe/zmq4"
)

var (
	errPermanent = errors.New("permanent error, abandoning request")
)

// Structure of our class
// We access these properties only via class methods

// Majordomo Protocol Client API.
type Client struct {
	broker  string
	client  *zmq.Socket   // Socket to broker
	verbose bool          // Print activity to stdout
	timeout time.Duration // Request timeout
	poller  *zmq.Poller
}

// ---------------------------------------------------------------------

// Connect or reconnect to broker. In this asynchronous class we use a
// DEALER socket instead of a REQ socket; this lets us send any number
// of requests without waiting for a reply.
func (c *Client) ConnectToBroker() (err error) {
	logger := log.GetLogger()

	if c.client != nil {
		c.client.Close()
		c.client = nil
	}
	c.client, err = zmq.NewSocket(zmq.DEALER)
	if err != nil {
		log.Error(logger, "msg", "ConnectToBroker() creating socket failed")
		return
	}
	c.poller = zmq.NewPoller()
	c.poller.Add(c.client, zmq.POLLIN)

	log.Debug(logger, "msg", "connecting", "broker", c.broker)

	err = c.client.Connect(c.broker)
	if err != nil {
		log.Error(logger, "msg", "ConnectToBroker() failed to connect", "broker", c.broker)
	}

	return
}

// Here we have the constructor and destructor for our client class:

// The constructor and destructor are the same as in mdcliapi, except
// we don't do retries, so there's no retries property.
// ---------------------------------------------------------------------
// Constructor

func NewClient(broker string) (c *Client, err error) {

	c = &Client{
		broker:  broker,
		timeout: time.Duration(2500 * time.Millisecond),
	}
	err = c.ConnectToBroker()
	runtime.SetFinalizer(c, (*Client).Close)
	return
}

// ---------------------------------------------------------------------
// Destructor

func (c *Client) Close() (err error) {
	if c.client != nil {
		err = c.client.Close()
		c.client = nil
	}
	return
}

// ---------------------------------------------------------------------

// Set request timeout.
func (c *Client) SetTimeout(timeout time.Duration) {
	c.timeout = timeout
}

// The send method now just sends one message, without waiting for a
// reply. Since we're using a DEALER socket we have to send an empty
// frame at the start, to create the same envelope that the REQ socket
// would normally make for us:
func (c *Client) Send(service string, request ...string) (err error) {
	logger := log.GetLogger()

	// Prefix request with protocol frames
	// Frame 0: empty (REQ emulation)
	// Frame 1: "MDPCxy" (six bytes, MDP/Client x.y)
	// Frame 2: Service name (printable string)

	req := make([]string, 3, len(request)+3)
	req = append(req, request...)
	req[2] = service
	req[1] = MDPC_CLIENT
	req[0] = ""
	log.Debug(logger, "msg", "send request", "service", service, "body", req)
	_, err = c.client.SendMessage(req)
	return
}

// The recv method waits for a reply message and returns that to the
// caller.
// ---------------------------------------------------------------------
// Returns the reply message or NULL if there was no reply. Does not
// attempt to recover from a broker failure, this is not possible
// without storing all unanswered requests and resending them all...

func (c *Client) Recv() (msg []string, err error) {
	logger := log.GetLogger()

	msg = []string{}

	// Poll socket for a reply, with timeout
	polled, err := c.poller.Poll(c.timeout)
	if err != nil {
		return // Interrupted
	}

	// If we got a reply, process it
	if len(polled) > 0 {
		msg, err = c.client.RecvMessage(0)
		if err != nil {
			log.Warn(logger, "msg", "interrupt received, killing client...")
			return
		}

		log.Debug(logger, "msg", "received reply", "body", msg)
		// Don't try to handle errors, just assert noisily
		if len(msg) < 4 {
			log.Error(logger, "msg", "len(msg) < 4")
			// TODO: consider appending error to the message to prevent crash below
			//msg = append(msg, "")
		}
		////else {
		//var empty string
		//empty, msg = util.PopStr(msg)
		//if empty != "" {
		if msg[0] != "" {
			log.Error(logger, "msg", "msg[0] != \"\"")
		}

		//var header string
		//header, msg = util.PopStr(msg)
		//if header != MDPC_CLIENT {
		if msg[1] != MDPC_CLIENT {
			log.Error(logger, "msg", "msg[1] != MDPC_CLIENT")
		}

		// FIXME: this fails when len(msg) < 4
		msg = msg[3:]
		//var service string
		//service, msg = util.PopStr(msg)
		//log.Debug(logger, "msg", "received message for service", "service", service)
		////}

		return // Success
	}

	err = errPermanent
	log.Error(logger, "msg", err)
	return
}
