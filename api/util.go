package api

func popWorker(workers []*brokerWorker) (worker *brokerWorker, workers2 []*brokerWorker) {
	worker = workers[0]
	workers2 = workers[1:]
	return
}

func delWorker(workers []*brokerWorker, worker *brokerWorker) []*brokerWorker {
	for i := 0; i < len(workers); i++ {
		if workers[i] == worker {
			workers = append(workers[:i], workers[i+1:]...)
			i--
		}
	}
	return workers
}
