package message

import (
	"testing"
)

func TestSettingsRequestMessage(t *testing.T) {
	if _, err := CreateSettingsRequest("read"); err != nil {
		t.Error(err)
	}
}

func TestSettingsResponseMessage(t *testing.T) {
	var reply = []byte(`{
		"msg-type": "response-message",
		"msg-body": {
			"body-type": "response-body",
			"body-props": [
				{ "key": "key-one", "value": "value-one" },
				{ "key": "key-two", "value": "value-two" },
				{ "key": "key-thr", "value": "value-thr" }
			]
		}
	}`)

	settings, err := CreateSettingsResponse(reply)
	if err != nil {
		t.Error(err)
	}

	if settings.Properties["key-one"] != "value-one" {
		t.Error("Wrong value for `key-one`")
	} else if settings.Properties["key-two"] != "value-two" {
		t.Error("Wrong value for `key-two`")
	} else if settings.Properties["key-thr"] != "value-thr" {
		t.Error("Wrong value for `key-thr`")
	}
}
