package message

import (
	"testing"
)

func TestStatusRequest(t *testing.T) {
	req := NewStatusRequest()
	if req.Name() != "request-status" {
		t.Error("wrong message name")
	}
}

func TestStatusRequestMessage(t *testing.T) {
	if _, err := CreateStatusRequest("read"); err != nil {
		t.Error(err)
	}
}

func TestStatusResponseMessage(t *testing.T) {
	var reply = []byte(`{
		"msg-type": "response-message",
		"msg-body": {
			"body-type": "response-body",
			"body-props": [
				{ "key": "status", "value": "ready" }
			]
		}
	}`)

	status, err := CreateStatusResponse(reply)
	if err != nil {
		t.Error(err)
	}

	if status.Functional != true {
		t.Error("Something went wrong")
	}
}

// -- new

func CreateMessage(msg interface{}) (bytes []byte, err error) {

	return
}

func TestReadStatus(t *testing.T) {
	rs := &ReadStatus{}
	json, err := PackMessage(rs)
	msg, err := UnpackMessage(json)
	switch msg.(type) {
	case ReadStatus:
		break
	default:
		t.Error("Wrong message type received")
	}
}
