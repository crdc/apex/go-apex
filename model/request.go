package model

// TODO: add field for recipient (uri?)
// TODO: add field for service expecting response

type RequestMessage struct {
	Type string      `json:"msg-type"`
	Body RequestBody `json:"msg-body,omitempty"`
}

type RequestBody struct {
	Type       string     `json:"body-type"`
	Properties []Property `json:"body-props,omitempty"`
}
