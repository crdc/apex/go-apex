package model

import (
	"bytes"
	"encoding/json"
	"testing"
)

func TestMarshalProperty(t *testing.T) {
	var decoded = []byte(`{"key":"status","value":"ready"}`)
	prop := &Property{
		Key:   "status",
		Value: "ready",
	}
	b, err := json.Marshal(prop)
	if err != nil {
		t.Errorf("Failed to marshal data: %v\n", err)
	}
	if !bytes.Equal(b, decoded) {
		t.Errorf("Didn't receive expected bytes\n")
	}
}

func TestMarshalRequestMessage(t *testing.T) {
	var decoded = []byte(`{"msg-type":"request-message","msg-body":{"body-type":"request-body","body-props":[{"key":"status","value":"read"}]}}`)
	var props []Property
	props = append(props, Property{
		Key:   "status",
		Value: "read",
	})

	body := RequestBody{
		Type:       "request-body",
		Properties: props,
	}

	req := &RequestMessage{
		Type: "request-message",
		Body: body,
	}

	b, err := json.Marshal(req)
	if err != nil {
		t.Errorf("Failed to marshal data: %v\n", err)
	}
	if !bytes.Equal(b, decoded) {
		t.Errorf("Didn't receive expected bytes\n")
	}
}
