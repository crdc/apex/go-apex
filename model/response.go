package model

type ResponseMessage struct {
	Type string       `json:"msg-type"`
	Body ResponseBody `json:"msg-body,omitempty"`
}

type ResponseBody struct {
	Type       string     `json:"body-type"`
	Properties []Property `json:"body-props"`
}
