package model

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestUnpackRequestMessage(t *testing.T) {
	var blob = []byte(`{"msg-type": "request-message"}`)
	u := &Unpacker{}
	if err := json.Unmarshal(blob, u); err != nil {
		t.Error("Failed to unmarshal data")
	}
	var msg = u.Data.(*RequestMessage)
	if msg.Type != "request-message" {
		t.Errorf("Received invalid message type: %s\n", msg.Type)
	}
	if reflect.TypeOf(msg).String() != "*model.RequestMessage" {
		t.Error("Wrong type, received: ", reflect.TypeOf(msg).String())
	}
}

func TestUnpackResponseMessage(t *testing.T) {
	var blob = []byte(`{"msg-type": "response-message"}`)
	u := &Unpacker{}
	if err := json.Unmarshal(blob, u); err != nil {
		t.Error("Failed to unmarshal data")
	}
	var msg = u.Data.(*ResponseMessage)
	if msg.Type != "response-message" {
		t.Errorf("Received invalid message type: %s\n", msg.Type)
	}
	if reflect.TypeOf(msg).String() != "*model.ResponseMessage" {
		t.Error("Wrong type, received: ", reflect.TypeOf(msg).String())
	}
}

func TestUnpackResponseMessageWithBody(t *testing.T) {
	var blob = []byte(`{
		"msg-type": "response-message",
		"msg-body": {
			"body-type": "response-body",
			"body-props": [
				{ "key": "status", "value": "ready" }
			]
		}
	}`)
	u := &Unpacker{}
	if err := json.Unmarshal(blob, u); err != nil {
		t.Error("Failed to unmarshal data")
	}
	var msg = u.Data.(*ResponseMessage)
	var body = msg.Body
	if body.Type != "response-body" {
		t.Errorf("Received invalid body type: %s\n", body.Type)
	}
	for _, prop := range body.Properties {
		if prop.Key == "status" && prop.Value != "ready" {
			t.Errorf("Incorrect %s received: %s\n", prop.Key, prop.Value)
		}
	}
	if msg == nil {
		t.Error("Received empty message")
	}
}

func TestUnpackRequestMessageWithBody(t *testing.T) {
	var blob = []byte(`{
		"msg-type": "request-message",
		"msg-body": {
			"body-type": "request-body",
			"body-props": [
				{ "key": "status", "value": "read" }
			]
		}
	}`)
	u := &Unpacker{}
	if err := json.Unmarshal(blob, u); err != nil {
		t.Error("Failed to unmarshal data")
	}
	var msg = u.Data.(*RequestMessage)
	var body = msg.Body
	if body.Type != "request-body" {
		t.Errorf("Received invalid body type: %s\n", body.Type)
	}
	for _, prop := range body.Properties {
		if prop.Key == "status" && prop.Value != "read" {
			t.Errorf("Incorrect %s received: %s\n", prop.Key, prop.Value)
		}
	}
	if msg == nil {
		t.Error("Received empty message")
	}
}
